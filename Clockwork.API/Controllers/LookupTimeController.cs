﻿using System;
using Microsoft.AspNetCore.Mvc;
using Clockwork.API.Models;
using System.Collections.Generic;
using System.Net.Http;

namespace Clockwork.API.Controllers
{
    [Route("api/[controller]")]
    public class LookupTimeController : Controller
    {
        // GET api/lookuptime
        [HttpGet]
        public IActionResult Get()
        {
            List<CurrentTimeQuery> oTimeQuery = new List<CurrentTimeQuery>();

            using (var db = new ClockworkContext())
         

                foreach (var CurrentTimeQuery in db.CurrentTimeQueries)
                {
                    oTimeQuery.Add(CurrentTimeQuery);
                }

                oTimeQuery.Reverse();

                return Ok(oTimeQuery);
         
            }
        }
    }
