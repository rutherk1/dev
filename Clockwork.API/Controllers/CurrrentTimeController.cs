﻿using System;
using Microsoft.AspNetCore.Mvc;
using Clockwork.API.Models;
using Microsoft.EntityFrameworkCore;

namespace Clockwork.API.Controllers
{
    [Route("api/[controller]/{TZ}")]
    public class CurrentTimeController : Controller
    {
        // GET api/currenttime  
        [HttpGet]
        public IActionResult Get(String TZ)
        {
            var utcTime = DateTime.UtcNow;
            var serverTime = DateTime.Now;
            var ip = this.HttpContext.Connection.RemoteIpAddress.ToString();

            var returnVal = new CurrentTimeQuery
            {
                UTCTime = utcTime,
                ClientIp = ip,
                Time = serverTime
            };

         
            TimeZoneInfo tzInfo = TimeZoneInfo.FindSystemTimeZoneById(TZ);

            DateTime convertedTime = TimeZoneInfo.ConvertTimeFromUtc(returnVal.UTCTime, tzInfo);

            returnVal.Time = convertedTime;
            returnVal.TimeZone = TZ;

            using (var db = new ClockworkContext())
            {
                db.CurrentTimeQueries.Add(returnVal);
                var count = db.SaveChanges();
                Console.WriteLine("{0} records saved to database", count);

                Console.WriteLine();
                foreach (var CurrentTimeQuery in db.CurrentTimeQueries)
                {
                    Console.WriteLine(" - {0}", CurrentTimeQuery.UTCTime);
                }
            }

            return Ok(returnVal);
        }

    }
}
