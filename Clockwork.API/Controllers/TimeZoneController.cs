﻿using System;
using Microsoft.AspNetCore.Mvc;
using Clockwork.API.Models;
using System.Collections.Generic;
using System.Net.Http;

namespace Clockwork.API.Controllers
{
    [Route("api/[controller]")]
    public class TimeZoneController : Controller
    {
        // GET api/timezone
        [HttpGet]
        public IActionResult Get()
        {
            List<TimeZoneInfo> oTimeZoneInfo = new List<TimeZoneInfo>();

            var ServerTimeZones = TimeZoneInfo.GetSystemTimeZones();

            foreach (var timeZone in ServerTimeZones)
            {
                oTimeZoneInfo.Add(timeZone);
            }

            return Ok(oTimeZoneInfo);

        }
    }
}
